import { MainLayout } from '@/components/layouts/MainLayout/MainLayout'

export default function Contact() {
  return (
    <MainLayout title='Contact' description='Contact page of initial demo'>
      <div className='description'>
        <p>
          Get started by editing&nbsp;
          <code className='code'>pages/contact.tsx</code>
        </p>
      </div>
    </MainLayout>
  )
}
