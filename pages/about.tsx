import { MainLayout } from '@/components/layouts/MainLayout/MainLayout'

export default function About() {
  return (
    <MainLayout  title='About' description='About page of initial demo'>
      <div className='description'>
        <p>
          Get started by editing&nbsp;
          <code className='code'>pages/about.tsx</code>
        </p>
      </div>
    </MainLayout>
  )
}