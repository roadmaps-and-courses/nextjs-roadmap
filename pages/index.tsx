import { MainLayout } from '@/components/layouts/MainLayout/MainLayout'

export default function Home() {
  return (
    <MainLayout title='Home' description='Home page of initial demo'>
      <div className='description'>
        <p>
          Get started by editing&nbsp;
          <code className='code'>pages/index.tsx</code>
        </p>
      </div>
    </MainLayout>
  )
}
