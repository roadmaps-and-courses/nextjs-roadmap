import { useRouter } from "next/router"

import Link from "next/link"

interface ActiveLinkProps {
  href: string,
  children: any
}

const styles = {
  color: '#0070f3',
  textDecoration: 'underline'
}

export const ActiveLink = ({href, children}: ActiveLinkProps) => {

  const {asPath} = useRouter();

  return (
    <Link href={href} style={asPath === href ? styles : {}}>
      {children}
    </Link>
  )
}
