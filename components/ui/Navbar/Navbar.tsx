import Link from 'next/link'
import styles from './Navbar.module.css'
import { ActiveLink } from '../ActiveLink/ActiveLink'

const routes = [
  {href: '/', content: 'Home'},
  {href: '/about', content: 'About'},
  {href: '/contact', content: 'Contact'},
]
export const Navbar = () => {
  return (
    <nav className={styles.navbar}>
      {routes.map(({href,content}) => <ActiveLink href={href} key={href}>{content}</ActiveLink>)}
    </nav>
  )
}
