import { Navbar } from '@/components/ui/Navbar/Navbar'
import { Inter } from 'next/font/google'
import Head from 'next/head'

import styles from './MainLayout.module.css'

interface MainLayoutProps {
  title: string,
  description: string,
  children: any
}

const inter = Inter({ subsets: ['latin'] })

export const MainLayout = ({title, description, children}: MainLayoutProps) => {
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar/>
      <main className={`${styles.main} ${inter.className}`}>
        {children}
      </main>
    </>
  )
}
